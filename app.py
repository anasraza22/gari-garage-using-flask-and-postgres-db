from flask import *
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from random import randint
from flask_mail import Mail

app = Flask(__name__)
app.secret_key = 'super-secret-key'
app.config[
    'SQLALCHEMY_DATABASE_URI'] = 'postgres://kenpmmxclgeoop:675cc3fe0f16397bd1de183e473c56caeb90f12f061f582d80c1e818e167f506@ec2-54-243-47-196.compute-1.amazonaws.com:5432/dfqk5aoaqir1b5'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config.update(
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT='465',
    MAIL_USE_SSL=True,
    MAIL_USERNAME='bitf16a022@gmail.com',
    MAIL_PASSWORD='Tempass@123'
)

mail = Mail(app)
db = SQLAlchemy(app)

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)


class Car(db.Model):
    __tablename__ = 'car'
    car_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    company_name = db.Column(db.String(200))
    price = db.Column(db.String(200))
    description = db.Column(db.String(1000))
    url = db.Column(db.String(1000))
    rating = db.Column(db.Integer)
    slug = db.Column(db.String(200), unique=True)


class Contact(db.Model):
    __tablename__ = 'contact'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    email = db.Column(db.String(200))
    subject = db.Column(db.String(200))
    message = db.Column(db.String(1000))


@app.route('/')
def index():
    cars = Car.query.all()
    no_of_cars = len(cars)
    if no_of_cars > 0:
        slide_cars = []
        for i in range(0, 6):
            rand_index = randint(0, no_of_cars - 1)
            rand_row = cars[rand_index]
            slide_cars.append(rand_row)

    rand_index = randint(0, no_of_cars - 1)
    main_car = cars[rand_index]
    car_brands = []
    for car in cars:
        car_brands.append(car.company_name)
    car_brands = list(set(car_brands))
    return render_template('index.html', cars=cars, car_brands=car_brands, slide_cars=slide_cars, main_car=main_car,
                           recent_cars=cars[0:6], no_of_cars=no_of_cars)


@app.route('/options')
def options():
    car_names = []
    comp_name = request.args.get('brand_name')
    cars = Car.query.filter_by(company_name=comp_name).all()
    for car in cars:
        car_names.append(car.name)
    items = {}
    for name in car_names:
        items[name] = name
    return items


@app.route('/ajax_search')
def ajax_search():
    comp_name = request.args.get('comp_name')
    car_name = request.args.get('car_name')
    car = Car.query.filter_by(name=car_name, company_name=comp_name).first()
    result = {'name': car.name,
              'company_name': car.company_name,
              'price': car.price,
              'description': car.description,
              'url': car.url,
              'rating': car.rating,
              'slug': car.slug}
    return result


@app.route('/search')
def search():
    cars = Car.query.all()
    item = request.args.get('query').lower()
    result = []
    for car in cars:
        if item in car.name.lower() or item in car.company_name.lower() or car.name.lower() in item or car.company_name.lower() in item:
            result.append(car)
    no_of_cars = len(result)
    return render_template('car_listing_sidebar.html', cars=result, no_of_cars=no_of_cars)


@app.route('/about_us')
def about_us():
    return render_template('about_us.html')


@app.route('/contact_us', methods=['GET', 'POST'])
def contact_us():
    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        subject = request.form['subject']
        message = request.form['message']
        info = Contact(name=name, email=email, subject=subject, message=message)
        db.session.add(info)
        db.session.commit()
        mail.send_message(subject,
                          sender=email,
                          recipients=["bitf16a022@gmail.com"],
                          body=message + '\n\nRegards,' + '\n' + name + '\n' + email + '\n\nThanks!')
    return render_template('contact_us.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if 'user' in session and session['user'] == 'Admin':
        cars = Car.query.all()
        return render_template('admin_pannel.html', cars=cars, enumerate=enumerate)

    if request.method == 'POST':
        username = request.form['username']
        pswrd = request.form['pswrd']
        if username == 'Admin' and pswrd == 'admin':
            session['user'] = username
            cars = Car.query.all()
            return render_template('admin_pannel.html', cars=cars, enumerate=enumerate)
        else:
            msg = "*Password incorrect"
            return render_template('login.html', msg=msg)
    return render_template('login.html')


@app.route('/logout', methods=['GET', 'POST'])
def logout():
    session.pop('user')
    return redirect('/login')


@app.route('/edit/<string:id>', methods=['GET', 'POST'])
def edit(id):
    if 'user' in session and session['user'] == 'Admin':
        if request.method == 'POST':
            name = request.form['name']
            comp_name = request.form['comp_name']
            price = request.form['price']
            descript = request.form['descript']
            url = request.form['url']
            rating = request.form['rating']
            slug = request.form['slug']

            if id == '0':
                car = Car(name=name, company_name=comp_name, price=price, description=descript, url=url, rating=rating,
                          slug=slug)
                db.session.add(car)
                db.session.commit()
            else:
                car = Car.query.filter_by(car_id=id).first()
                car.name = name
                car.company_name = comp_name
                car.price = price
                car.description = descript
                car.url = url
                car.rating = rating
                car.slug = slug
                db.session.commit()
            return redirect('/login')
    car = Car.query.filter_by(car_id=id).first()
    if id == '0':
        car = Car(car_id=0, name='', company_name='', price='', description='', url='', rating='', slug='')
    return render_template('add_edit.html', car=car)


@app.route('/delete/<string:id>')
def delete(id):
    if 'user' in session and session['user'] == 'Admin':
        car = Car.query.filter_by(car_id=id).first()
        db.session.delete(car)
        db.session.commit()
        return redirect('/login')


@app.route('/single_car/<string:car_slug>')
def single_car(car_slug):
    car = Car.query.filter_by(slug=car_slug).first()
    related_cars = Car.query.filter_by(company_name=car.company_name).all()
    return render_template('single_car.html', car=car, related_cars=related_cars)


@app.route('/car_listing_sidebar/<string:type>')
def car_listing_sidebar(type):
    cars = Car.query.all()
    car_brands = []
    for car in cars:
        car_brands.append(car.company_name)
    car_brands = list(set(car_brands))
    cars = []
    if type == 'simple':
        cars = Car.query.all()
    elif type == 'hotcars':
        cars = Car.query.filter_by(rating=5).all()
    elif type == 'audicars':
        cars = Car.query.filter_by(company_name='Audi').all()
    elif type == 'toyotacars':
        cars = Car.query.filter_by(company_name='Toyota').all()
    elif type == 'hondacars':
        cars = Car.query.filter_by(company_name='Honda').all()
    elif type == 'suzukicars':
        cars = Car.query.filter_by(company_name='Suzuki').all()
    no_of_cars = len(cars)
    return render_template('car_listing_sidebar.html', cars=cars, no_of_cars=no_of_cars, car_brands=car_brands)


@app.route('/list_search')
def list_search():
    comp_name = request.args.get('brand')
    car_name = request.args.get('car_list')
    print(comp_name, car_name)
    car = Car.query.filter_by(name=car_name, company_name=comp_name).first()
    car_slug = car.slug
    return redirect('/single_car/' + car_slug)


@app.route('/car_grid_sidebar/<string:type>')
def car_grid_sidebar(type):
    cars = Car.query.all()
    car_brands = []
    for car in cars:
        car_brands.append(car.company_name)
    car_brands = list(set(car_brands))
    cars = []
    if type == 'simple':
        cars = Car.query.all()
    elif type == 'hotcars':
        cars = Car.query.filter_by(rating=5).all()
    elif type == 'audicars':
        cars = Car.query.filter_by(company_name='Audi').all()
    elif type == 'toyotacars':
        cars = Car.query.filter_by(company_name='Toyota').all()
    elif type == 'hondacars':
        cars = Car.query.filter_by(company_name='Honda').all()
    elif type == 'suzukicars':
        cars = Car.query.filter_by(company_name='Suzuki').all()
    no_of_cars = len(cars)
    return render_template('car_grid_sidebar.html', cars=cars, no_of_cars=no_of_cars, car_brands=car_brands)


if __name__ == '__main__':
    # app.run(debug=True)
    manager.run()
